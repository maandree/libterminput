/* See LICENSE file for copyright and license details. */
#include "common.h"


extern inline int libterminput_is_ready(const union libterminput_input *input, const struct libterminput_state *ctx);
